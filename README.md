# ec2-clone
## Purpose
This repository gives examples of recreating ec2 functionalities.
However, it is done in such a way to be applied to other engineering
projects as well.  A few points of emphasis include idempotency, 
even when the server must generate an id (we do this using dynamodb 
conditional writes).  We also show a few neat technologies, such as cucumber
and cloudwatch dashboards (which display metrics written with emf).

## Associated medium article
TODO associated medium article link

## Deploying the infrastructure
For your first time deploying the stack, use the following command:
```
sam build && sam deploy --guided
```

For subsequent deploys, run
```
sam build && sam deploy
```
## Running tests & viewing result reports
To run the cucumber tests and generate a report for non-retries, run.  Run this first to verify it works and doesn't create metrics for retries yet:
```
npm run cucumberNonRetry
```

To run the cucumber tests and generate a report for retries, run.  Run this second and verify it creates metrics for retries:
```
npm run cucumberWithRetry
```

## Viewing stats in cloudwatch dashboard
Go to cloudwatch and look up the dashboard with the name *replica-ec2-control-plane*