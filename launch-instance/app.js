const AWS = require("aws-sdk")
const dynamodb = new AWS.DynamoDB({apiVersion: "2012-08-10"});
const { v4: generateUUID } = require('uuid');
const { createMetricsLogger, Unit } = require("aws-embedded-metrics");

const TABLE_NAME = process.env.TABLE_NAME;
const SECONDS_BEFORE_EXPIRATION = 60 * 5;

exports.lambdaHandler = async (event, context) => {
  //in the real world, you would perform some validation here
   const initialStartTimeMs = Date.now();
   const metrics = createMetricsLogger();
   metrics.setNamespace("replicaOfEc2");
   metrics.setDimensions({ }); //if we don't do this we will get log group name and other unwanted dimensions
  const eventBodyObject = JSON.parse(event.body);
  const clientToken = eventBodyObject.ClientToken
  metrics.setProperty("clientToken", clientToken);
  const instanceType = eventBodyObject.InstanceType;
  const reservationId = generateUUID().toString();
  const epochSeconds = (Math.round(Date.now() / 1000) + SECONDS_BEFORE_EXPIRATION).toString();
  const params = {
      TableName: TABLE_NAME,
      Item: {
        "clientToken": {
          S: clientToken
        },
        "reservationId": {
          S: reservationId
        },
        "instanceType": {
          S: instanceType
        },
        "expirationTime": {
          N: epochSeconds
        },
      },
      ConditionExpression: 'attribute_not_exists(clientToken)'
    };
    let response;
    const startTimeMs = Date.now();
    let isRetry = false;
    try {
      const result = await dynamodb.putItem(params).promise();
      const endTimeMs = Date.now();
      metrics.putMetric("insertSuccessLatency", endTimeMs - startTimeMs, Unit.Milliseconds);
      console.log('Results: ' + JSON.stringify(result));
  response = {
            'statusCode': 202,
            'body': JSON.stringify({"reservationId": reservationId})
        };
    } catch (err) {
    //NOTE: in a real world app, you would put a custom metric here, likely using emf
if(err.code === "ConditionalCheckFailedException"){
      isRetry = true;
      const endTimeMs = Date.now();
      metrics.putMetric("insertDuplicateLatency", endTimeMs - startTimeMs, Unit.Milliseconds);
    const reservationId = await getReservationIdForClientToken(clientToken, metrics);
    response = {
            'statusCode': 202,
            'body': JSON.stringify({"reservationId": reservationId})
    }
}else{
      console.log(err);
  response = {
            'statusCode': 500,
            'body': JSON.stringify({
                message: JSON.stringify(err)
            })
        };
}
}
const metricName = isRetry? "retryTotalLatency": "nonRetryTotalLatency";
const totalEndTimeMs = Date.now();
metrics.putMetric(metricName, totalEndTimeMs - initialStartTimeMs, Unit.Milliseconds);
metrics.flush();
return response;
};

async function getReservationIdForClientToken(clientToken, metrics){
 const params = {
  Key: {
   "clientToken": {
     S: clientToken
    }
  },
  TableName: TABLE_NAME
 };
   const startTimeMs = Date.now();
   const result = await getItem(params);
   const endTimeMs = Date.now();
   metrics.putMetric("retrieveExistingLatency", endTimeMs - startTimeMs, Unit.Milliseconds);
   const reservationId = result.Item.reservationId.S;
   return reservationId;
}

async function getItem(params){
    try{
        return await dynamodb.getItem(params).promise();
    } catch (err) {
        err.message = "Error getting item for retry " + JSON.stringify(params) + " error message " + err.message;
        throw err;
    }
}
