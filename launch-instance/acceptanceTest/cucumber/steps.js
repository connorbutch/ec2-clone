const { v4: generateUUID } = require('uuid');
const { Before, Given, When, Then } = require('cucumber');
const assert = require('assert')
const axios = require('axios')

const BASE_URL = process.env.BASE_URL;

let scenario;
let instanceType;
let clientToken;
let responseOne;
let responseTwo;
let errorResponse;

Before(function (passedScenario/*, callback*/) {
    //world.scenario = scenario;
    //TODO figure out how to use scenario here
    scenario = passedScenario;
    instanceType = null;
    clientToken = null;
    responseOne = null;
    responseTwo = null;
    errorResponse = null;
});

Given('an instance type of {string} is being requested to be launched', function (passedInstanceType) {
    instanceType = passedInstanceType;
});

Given('a clientToken of {string} is being sent in the request', function (passedClientToken) {
    clientToken = passedClientToken;
});

Given('a random clientToken is being sent in the request', function () {
    clientToken = generateUUID();
});

Given('an instance type of {string}{string} is being requested to be launched', function (firstPartOfInstanceType, secondPartOfInstanceType) {
    instanceType = firstPartOfInstanceType + "." + secondPartOfInstanceType;
});

Given('an instance type of {string}   {string} is being requested to be launched', function (firstPartOfInstanceType, secondPartOfInstanceType) {
    instanceType = firstPartOfInstanceType + "." + secondPartOfInstanceType;
});

Given('a clientToken of {string}{string} is being sent in the request', function (firstPartOfClientToken, secondPartOfClientToken) {
    clientToken = firstPartOfClientToken + "." + secondPartOfClientToken;
});

Given('a clientToken of {string}    {string} is being sent in the request', function (firstPartOfClientToken, secondPartOfClientToken) {
    clientToken = firstPartOfClientToken + "." + secondPartOfClientToken;
});

Given('an instance type of m4.xlarge is being requested to be launched', function () {
    instanceType = "m4.xlarge";
});


When('two requests with different client tokens are made to launch the instance', async function () {
const requestBody = {
            ClientToken: clientToken,
            InstanceType: instanceType
        };
        console.log(requestBody);
        console.log(BASE_URL);
        try{
            responseOne = await axios.post(BASE_URL, requestBody);
            console.log("Successfully created with response body ", responseOne.data);
            console.log("response status code ", responseOne.status);
        }catch(err){
            //NOTE: this is not always a bad thing -- we need to run negative test cases for 400s as well
            console.log(err);
            errorResponse = err;
        }

            requestBody.ClientToken = generateUUID();
            try{
                responseTwo = await axios.post(BASE_URL, requestBody);
                console.log("Successfully created with response body ", responseTwo.data);
                console.log("response status code ", responseTwo.status);
            }catch(err){
                //NOTE: this is not always a bad thing -- we need to run negative test cases for 400s as well
                console.log(err);
                errorResponse = err;
            }
});

When('two identical requests are made to launch the instance', async function () {
    const requestBody = {
            ClientToken: clientToken,
            InstanceType: instanceType
        };
        console.log(requestBody);
        console.log(BASE_URL);
        try{
            responseOne = await axios.post(BASE_URL, requestBody);
            console.log("Successfully created with response body ", responseOne.data);
            console.log("response status code ", responseOne.status);
        }catch(err){
            //NOTE: this is not always a bad thing -- we need to run negative test cases for 400s as well
            console.log(err);
            errorResponse = err;
        }

            try{
                responseTwo = await axios.post(BASE_URL, requestBody);
                console.log("Successfully created with response body ", responseTwo.data);
                console.log("response status code ", responseTwo.status);
            }catch(err){
                //NOTE: this is not always a bad thing -- we need to run negative test cases for 400s as well
                console.log(err);
                errorResponse = err;
            }
});

Then('the response status code will be {int}', function (expectedHttpStatusCode) {
    const actualHttpStatusCode = responseOne == null? errorResponse.status: responseOne.status;
    assert.equal(actualHttpStatusCode, expectedHttpStatusCode)
});

Then('the response body will contain an error message {string}', function (string) {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('the response bodies will have a different ids', function () {
assert.notEqual(responseOne.data.reservationId, responseTwo.data.reservationId)
});

Then('both response bodies will contain the same id', function () {
assert.equal(responseOne.data.reservationId, responseTwo.data.reservationId)
});

Then('both responses will have a status code of {int}', function (expectedHttpStatusCode) {
    assert.equal(responseOne.status, expectedHttpStatusCode)
    assert.equal(responseTwo.status, expectedHttpStatusCode)
});