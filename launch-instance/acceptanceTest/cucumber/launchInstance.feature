Feature: Ec2 launch instances

#  @NegativeTest
#  Scenario Outline: Attempting to launch an instance with an improper input should return an error response with appropriate status code and response body
#    Given an instance type of "<instanceType>" is being requested to be launched
#    And a clientToken of "<clientToken>" is being sent in the request
#    When a request is made to launch the instance
#    Then the response status code will be 400
#      #you would really use a code within the response body here to be more specific
#    And the response body will contain an error message "<errorMessage>"
#    Examples:
#      | instanceType | clientToken | errorMessage                               |
#      | null         | abc         | instance type must not be null             |
#      | ""           | abc         | instance type must not be the empty string |
#      | "   "        | abc         | instance type must not be a blank string   |
#      | t2.large     | null        | clientToken must not be null               |
#      | t2.large     | ""          | clientToken must not be the empty string   |
#      | t2.large     | "    "      | clientToken must not be a blank string     |


    @Retry
  Scenario Outline: Requesting to launch an instance multiple times (one original request and then a retry) with the same clientToken should result in a success response with the same id being returned
    Given an instance type of "<instanceType>" is being requested to be launched
    And a clientToken of "<clientToken>" is being sent in the request
    When two identical requests are made to launch the instance
    Then both responses will have a status code of 202
    And both response bodies will contain the same id
    Examples:
      | instanceType | clientToken                          |
      | r4.8xlarge   | e65f98f8-f9cc-4176-a540-41b039ef4e36 |

  @NonRetry
  Scenario: Requesting to launch two different instances multiple times with the different clientToken should result in a success response and two different id being returned
    Given an instance type of m4.xlarge is being requested to be launched
    And a random clientToken is being sent in the request
    When two requests with different client tokens are made to launch the instance
    Then both responses will have a status code of 202
    And the response bodies will have a different ids